console.log("The WORLD!");

// Conditional Statements

// Conditional statements allow us to perform tasks based on a condition.

let num1 = 0;

// If statement - if statement allow us to perform a task IF the condition given is true.

/*

	if(condition){
		task/code to perform
	}

*/

// runs code if the condition is true
if(num1 === 0){
	console.log("The value of num1 is 0");
}

num1 = 25


// code does not run because the condition is now false.
if(num1 ===0){
	console.log("The current value of num1 is still 0");
}

let city = "New York";

// can we show to message in the console?
// No. Because condition is not met.
if(city === "New Jersey"){
	console.log("Welcome to New Jersey");
}

// How can we add a response to a failed condition?

// else - executes a code/task if the if previous condition/s are not met.

if(city === "New Jersey"){
	console.log("Welcome to New Jersey");
} else {
	console.log("This is not New Jersey!");
}

if(num1 < 20){
	console.log("num1's value is less than 20");
} else {
	console.log("num1's value is more than 20");
}

// Can we use if-else in a function?
// Yes. This improves reusability of our code.

function cityChecker(city){

	if(city === "New York"){
		console.log("Welcome to the Empire State!");
	} else {
		console.log("You're not in Neww York!");
	}
}

cityChecker("New York");
cityChecker("Los Angeles");

// Mini Activity

function printBudget(budgetNumber){
	if(budgetNumber >= 40000){
		console.log("You're still within budget");
	} else {
		console.log("You are currently over budget")
	}
};

printBudget(25000);
printBudget(50000)

// can we then, if a condition is not met, show a different response if another specified condition is met instead? 

// Else if
// Allow us to execute code/task if the previous condition/s are not met or false and IF the specified condition is met instead

let city2 = "Manila"

if(city2 === "New York"){
	console.log("Welcome to New York");
} else if(city2 === "Manila"){
	console.log("Welcome to Manila!");
} else {
	console.log("I don't know where your are.");
}

// else and else if statements are optional. Meaning in a conditional chain/statement, there should always be an if statement. You can skip or not add else if or else statements.

// if(city2 === "New York"){
// 	console.log("New York City!");
// }
// else if(city2 === "Manila"){
// 	console.log("I keep coming back to Manila.");
// }
// else {
// 	console.log("Where's waldo?");
// }

// Usually we only have a single else statement. Because Else is run when ALL conditions have not been met.

// let role = "admin";

// if(role === "developer"){
// 	console.log("Welcome back, developer");
// } else {
// 	console.log("Role provided is invalid.");
// } else if(role === "admin") {
// 	console.log("Hello, Admin");
// }


// Multiple Else If Statements

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return "Not a typhoon yet"
	} else if(windspeed <=60){
		return "Tropical Depresson Detected."
	} else if(windspeed >= 62 && windspeed <= 88){
		return "Tropical Storm Detected.";
	} else if(windspeed >= 89 || windspeed <= 117){
		return "Tropical Storm Detected.";
	} else {
		return "Typhoon Detected."
	}
}

let typhoonMessage1 = determineTyphoonIntensity(29);
let typhoonMessage2 = determineTyphoonIntensity(63);
let typhoonMessage3 = determineTyphoonIntensity(61);
let typhoonMessage4 = determineTyphoonIntensity(88);
let typhoonMessage5 = determineTyphoonIntensity(117);
let typhoonMessage6 = determineTyphoonIntensity(120);

console.log(typhoonMessage1);
console.log(typhoonMessage2);
console.log(typhoonMessage3);
console.log(typhoonMessage4);
console.log(typhoonMessage5);
console.log(typhoonMessage6);

// Truthy and Falsy values

// In JS, there are values that are considered "truthy", which means in a boolean context, like determining an if condition, it is considered true.

// Samples if Truthy
// 1. true
// 2.
if(1){
	console.log("1 is truthy");
}
// 3.
if([]){
	// Even though the array is empty, it already exists, it is an existing instance of an Array.
	console.log("[] empty array is truthy");
}

// Falsy values are values considered "false" in a boolean context like determining an if condition

// 1.false

// 2.
if(0){
	console.log("0 is falsy.");
}

// 3.
if(undefined){
	console.log("undefined is not falsy");
} else {
	console.log("undefined is falsy");
}

// Conditional Ternary Operator
// Ternary operator is used as a shorter alternative to if else statements
// It is also able to implicitly return a value. Meaning it does not have to use the return keyword to return a value.

// syntax: (condition) ? ifTrue : ofFalse;

let age = 17;
let result = age < 18 ? "Underage" : "Legal Age";
console.log(result);

// let result2 = if(age < 18){
// 	return "Underage";
// } else {
// 	return "Legal Age";
// }

// console.log(result2);

// Switch Statement
// Evaluate an expression and match the expression to a case clause.
// An expression will be compared against different cases. Then, we will be ablt to run code IF the expression being evaluated matches a case.
// It is used alternatively from an if-else statement. However, if-else statements provides more complexity in its conditions
// .toLowercase method is a built-in JS method which converts a string to lowercase.

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

// Switch statement to evaluate the current day and show a message to tell the user the color of the day.

switch(day){

	case 'monday':
		console.log("The color of the day is red.");
		break;
	case 'tuesday':
		console.log("The color of the day is orange.");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow.");
		break;
	case 'thursday':
		console.log("The color of the day is blue.");
		break;
	case 'friday':
		console.log("The color of the day is green.");
		break;
	case 'saturday':
		console.log("The color of the day is violet.");
		break;
	case 'sunday':
		console.log("The color of the day is black.");
		break;
	default:
		console.log("Please enter a valid day");
		break;
}

// Try-Catch-Finally
// We use the try-catch-finally statements to cath errors, display and inform about the error and continue the code instead of stopping.
// We could also use the try-catch-finally statements to produce our own error messages in the event of an error.
// try-catch-finally is used to anticipate, catch and inform about an error.

// try allows us to run code, if there is an error in the instance of our code, then we will be able to catch that error in our catch statement.
try{

	alert(determineTyphoonIntensity(50));

// the error is passed into the catch statement. Developers usually name the error as "error", err or even "e"
} catch (error){

	// With the use of the typeof keyword, we will be able to return string which contains information to what data type our error is.

	// error is an object with methods and properties that describe the error
	// error.message allows us to access information about the error
	console.log(error.message);

	// With a try-catch statement we can catch errors and instead of stopping the whole program, we can catch the error and continue.

	// This is what we call Error Handling.
} finally {

	// Finally staetment will run regardless of the success or failure of the try statement
	alert("Intensity updates will show in a new alert");
}

console.log("Will we continue to the next code?");
