console.log("Hello World");

// login function
function login(){

	let username = prompt("Please input your  username");
	let password = prompt("Please input your password");
	let role = prompt("Please input your role");

	if(username === null || username === ""){
		alert("Your username should not be empty");
	}
	if (password === null || username === ""){
		alert("Your password should not be empty");
	} 
	if (role === null || username === ""){
		alert("Your role should not be empty");
	} else{
		switch(role){
			case 'admin':
			alert("Welcome back to the class portal, admin!");
			break;

			case 'teacher':
			alert("Thank you for logging in, teacher!");
			break;

			case 'student':
			alert("Welcome to the class portal, student!");
			break;

			default:
			alert("Role out of range.");
		}
	}
};
login();


// average function
function checkAverage(num1,num2,num3,num4){
	let average = (num1+num2+num3+num4) / 4;
	let averageUpdated = Math.round(average);

	if(averageUpdated <= 74){
		console.log("Hello student, your average is: " + average + "." + " The letter equivalent is F");
	} else if(averageUpdated >= 75 && averageUpdated <= 79){
		console.log("Hello student, your average is: " + average + "." + " The letter equivalent is D");
	} else if(averageUpdated >= 80 && averageUpdated <= 84){
		console.log("Hello student, your average is: " + average + "." + " The letter equivalent is C");
	} else if(averageUpdated >= 85 && averageUpdated <= 89){
		console.log("Hello student, your average is: " + average + "." + " The letter equivalent is B");
	} else if(averageUpdated >= 90 && averageUpdated <= 95){
		console.log("Hello student, your average is: " + average + "." + " The letter equivalent is A");
	} else if(averageUpdated >= 96){
		console.log("Hello student, your average is: " + average + "." + " The letter equivalent is A+");
	}
};

checkAverage();